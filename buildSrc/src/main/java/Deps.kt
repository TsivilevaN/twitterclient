class Deps {

    companion object {
        private val ROOM_VERSION = "2.2.5"
        private val KOTLIN_COROUTINES_VERSION = "1.1.1"
        private val RETROFIT_VERSION = "2.8.1"
        private val KOIN_VERSION = "2.1.0"
        private val GSON_VERSION = "2.8.5"
        private val CARD_VIEW_VERSION = "1.0.0"
        private val CONSTRAINT_VERSION = "1.1.3"
        private val ESPRESSO_VERSION = "3.2.0"
        private val JUNIT_EXT_VERSON = "1.1.1"
        private val ANDROID_CORE_VERSION = "1.2.0"
        private val SUPPORT_LIB_VERSION = "1.0.0"
        private val KOTLIN_VERSION = "1.3.61"
        private val RECYCLER_VIEW_VERSION = "1.1.0"
        private val NAV_VERSION = "2.2.1"
        private val GLIDE_VERSION = "4.11.0"
        private val LOTTIE_VERSION = "3.4.0"
        private val FIREBASE_ANALYTICS_VERSION="17.4.2"
        private val FIREBASE_AUTH_VERSION="19.3.1"

        val kotlin = "org.jetbrains.kotlin:kotlin-stdlib-jdk7:$KOTLIN_VERSION"
        val supportAppCompat = "androidx.appcompat:appcompat:$SUPPORT_LIB_VERSION"

        val androidCore = "androidx.core:core-ktx:$ANDROID_CORE_VERSION"

        val junit = "junit:junit:4.12"
        val junitExt = "androidx.test.ext:junit:$JUNIT_EXT_VERSON"

        val espresso = "androidx.test.espresso:espresso-core:$ESPRESSO_VERSION"


        val constraintLayout = "androidx.constraintlayout:constraintlayout:$CONSTRAINT_VERSION"
        val recyclerView = "androidx.recyclerview:recyclerview:$RECYCLER_VIEW_VERSION"
        val legacySupport = "androidx.legacy:legacy-support-v4:1.0.0"
        val materialSupport = "com.google.android.material:material:1.1.0"

        val cardView = "androidx.cardview:cardview:$CARD_VIEW_VERSION"
        val circleImageView = "de.hdodenhof:circleimageview:3.1.0"

        val navComp = "androidx.navigation:navigation-fragment-ktx:" + NAV_VERSION
        val navCompUI = "androidx.navigation:navigation-ui-ktx:" + NAV_VERSION

        val gson = "com.google.code.gson:gson:" + GSON_VERSION

        val koin = "org.koin:koin-android:" + KOIN_VERSION
        val koinScope = "org.koin:koin-androidx-scope:" + KOIN_VERSION
        val koinViewModel = "org.koin:koin-androidx-viewmodel:" + KOIN_VERSION

        val retrofit = "com.squareup.retrofit2:retrofit:" + RETROFIT_VERSION
        val retrofitGsonConverter = "com.squareup.retrofit2:converter-gson:" + RETROFIT_VERSION
        val retrofitSerialization =
            "com.jakewharton.retrofit:retrofit2-kotlinx-serialization-converter:0.3.0"
        val retrofitLogging = "com.squareup.okhttp3:logging-interceptor:3.6.0"


        val kotlinCoroutinesCore =
            "org.jetbrains.kotlinx:kotlinx-coroutines-core:" + KOTLIN_COROUTINES_VERSION
        val kotlinCoroutinesAndroid =
            "org.jetbrains.kotlinx:kotlinx-coroutines-android:" + KOTLIN_COROUTINES_VERSION


        val twitterKit = "com.twitter.sdk.android:twitter:3.3.0"
        val OAuth = "com.github.scribejava:scribejava-apis:6.4.1"

        val room = "androidx.room:room-runtime:$ROOM_VERSION"
        val roomCompiler ="androidx.room:room-compiler:$ROOM_VERSION"
        val roomCompilerPersistence ="android.arch.persistence.room:compiler:1.1.1"
        val roomCoroutines = "androidx.room:room-ktx:$ROOM_VERSION"
        val roomRxJava = "androidx.room:room-rxjava2:$ROOM_VERSION"
        val roomTest = "androidx.room:room-testing:$ROOM_VERSION"

        val glide = "com.github.bumptech.glide:glide:${GLIDE_VERSION}"
        val glideProcessor = "com.github.bumptech.glide:compiler:${GLIDE_VERSION}"
        val glideRecyclerViewIntegration ="com.github.bumptech.glide:recyclerview-integration:${GLIDE_VERSION}"
        val glideOkHttpIntegration = "com.github.bumptech.glide:okhttp3-integration:${GLIDE_VERSION}"

        val lottie="com.airbnb.android:lottie:$LOTTIE_VERSION"

        val pagination="androidx.paging:paging-runtime:2.1.2"

        val swiper="androidx.swiperefreshlayout:swiperefreshlayout:1.0.0"


        val firebaseAnalytic="com.google.firebase:firebase-analytics:$FIREBASE_ANALYTICS_VERSION"
        val firebaseAnalyticKtx="com.google.firebase:firebase-analytics-ktx:$FIREBASE_ANALYTICS_VERSION"

        val firebaseAuthorization="com.google.firebase:firebase-auth:$FIREBASE_AUTH_VERSION"
        val firebaseAuthorizationKtx="com.google.firebase:firebase-auth-ktx:$FIREBASE_AUTH_VERSION"
    }
}