package com.systems.magora.search

import androidx.lifecycle.MutableLiveData
import androidx.paging.DataSource
import com.systems.magora.twitter.core.model.locale.User
import com.systems.magora.twitter.networking.RemoteRepositoryImpl
import kotlinx.coroutines.CoroutineScope

//TODO : В Koin, при создании объекта-фабрики передат в конструктор параметр для запроса (query)
class SearchDataSourceFactory(
    private val scope: CoroutineScope,
    private val repository: RemoteRepositoryImpl,
    var requestText: String = ""
) : DataSource.Factory<Int, User>() {

    var liveData = MutableLiveData<SearchDataSource>()


    override fun create(): DataSource<Int, User> {
        val dataSource = SearchDataSource(scope, repository)
        dataSource.searchingName=requestText
        liveData.postValue(dataSource)
        return dataSource
    }
}