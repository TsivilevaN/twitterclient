package com.systems.magora.search

import androidx.recyclerview.widget.DiffUtil
import com.systems.magora.twitter.core.model.locale.User

class SearchDiffUtilCallbaks : DiffUtil.ItemCallback<User>() {
    override fun areItemsTheSame(oldItem: User, newItem: User) = oldItem.userId == newItem.userId
    override fun areContentsTheSame(oldItem: User, newItem: User)= newItem == oldItem
}