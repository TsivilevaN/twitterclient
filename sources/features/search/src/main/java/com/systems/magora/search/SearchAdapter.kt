package com.systems.magora.search

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.systems.magora.twitter.core.GlideApp
import com.systems.magora.twitter.core.model.locale.User
import de.hdodenhof.circleimageview.CircleImageView

class SearchAdapter() :
    PagedListAdapter<User, SearchAdapter.SearchViewHolder>(SearchDiffUtilCallbaks()) {
    interface OnUserClickListener {
        fun onUserClick(position: Int, view: View?)
    }

    var listener: OnUserClickListener? = null


    class SearchViewHolder(itemView: View, val listener: OnUserClickListener?) :
        RecyclerView.ViewHolder(itemView), View.OnClickListener {
        var nameTextView: TextView
        var shortNameTextView: TextView
        var imageCircleView: CircleImageView

        init {
            nameTextView = itemView.findViewById(R.id.name_text_view)
            shortNameTextView = itemView.findViewById(R.id.short_name_text_view)
            imageCircleView = itemView.findViewById(R.id.avatar_circle_image_view)
        }

        override fun onClick(p0: View?) {
            listener?.onUserClick(adapterPosition, p0)
        }

        fun bind(user: User) {
            nameTextView.text = user.userName
            shortNameTextView.text = user.screenName
            GlideApp.with(imageCircleView)
                .load(user.avatarUrl)
                .centerCrop()
                .into(imageCircleView)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SearchViewHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.item_searched_user, parent, false)
        return SearchViewHolder(view, listener)
    }

    // override fun getItemCount() = searchedUser.count()

    override fun onBindViewHolder(holder: SearchViewHolder, position: Int) {
        getItem(position)?.let { holder.bind(it) }
        /*   holder.nameTextView.text=searchedUser[position].userName
    holder.shortNameTextView.text=searchedUser[position].screenName
        GlideApp.with(holder.imageCircleView)
            .load(searchedUser[position].avatarUrl)
            .centerCrop()
            .into(holder.imageCircleView)
*/
    }
}