package com.systems.magora.search.di

import com.systems.magora.search.SearchViewModel
import com.systems.magora.twitter.core.BaseModuleLoader
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val searchUseCase= module{
    viewModel { SearchViewModel(reppository=get(),application = get()) }
  }

internal object searchModuleLoader: BaseModuleLoader(listOf(searchUseCase))