package com.systems.magora.search

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.paging.PageKeyedDataSource
import com.systems.magora.twitter.core.*
import com.systems.magora.twitter.core.model.locale.User
import com.systems.magora.twitter.networking.RemoteRepositoryImpl
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch

class SearchDataSource(val scope: CoroutineScope, val repository: RemoteRepositoryImpl) :
    PageKeyedDataSource<Int, User>() {

    var searchingName = ""
    var status = MutableLiveData<PageDownloadingStatus>()

    override fun loadInitial(
        params: LoadInitialParams<Int>,
        callback: LoadInitialCallback<Int, User>
    ) {
        val count = params.requestedLoadSize
        Log.d("search_log","loadInitial() pageNumber=1, count=$count")

        status.postValue(PageLoading)

        scope.launch {
            val networResonce = repository.searchUser(searchingName, count, 1)
            when (networResonce) {
                is NetworkSuccessfull -> {
                    status.postValue(PageSuccessful)
                    callback.onResult(networResonce.data, 0,count,null, 2)
                }
                is NetworkError -> {
                    status.postValue(PageError)
                    when (networResonce.cachedData) {
                        null -> callback.onResult(emptyList(), null, 2)
                        else -> callback.onResult(networResonce.cachedData!!, 0, count,null, 2)
                    }

                }

            }
        }
    }

    override fun loadAfter(params: LoadParams<Int>, callback: LoadCallback<Int, User>) {
        val count = params.requestedLoadSize
        val pageNumber = params.key
        Log.d("search_log","loadAfter() pageNumber=$pageNumber, count=$count")
        status.postValue(PageLoading)

        scope.launch {
            val networResonce = repository.searchUser(searchingName, count, pageNumber)
            when (networResonce) {
                is NetworkSuccessfull -> {
                    status.postValue(PageSuccessful)
                    callback.onResult(networResonce.data, pageNumber + 1)
                }
                is NetworkError -> {
                    status.postValue(PageError)
                    when (networResonce.cachedData) {
                        null -> callback.onResult(emptyList(), pageNumber + 1)
                        else -> callback.onResult(networResonce.cachedData!!, pageNumber + 1)
                    }

                }

            }
        }
    }

    override fun loadBefore(params: LoadParams<Int>, callback: LoadCallback<Int, User>) {
    //nothing to DO
        }

}