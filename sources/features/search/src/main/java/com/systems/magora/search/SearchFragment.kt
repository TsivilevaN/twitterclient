package com.systems.magora.search

import android.content.Context
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.paging.PagedList
import androidx.recyclerview.widget.LinearLayoutManager
import com.systems.magora.search.di.searchModuleLoader
import com.systems.magora.twitter.core.PageDownloadingStatus
import com.systems.magora.twitter.core.PageError
import com.systems.magora.twitter.core.PageLoading
import com.systems.magora.twitter.core.PageSuccessful
import com.systems.magora.twitter.core.model.locale.User
import kotlinx.android.synthetic.main.fragment_search.*
import kotlinx.android.synthetic.main.fragment_search.view.*
import org.koin.androidx.viewmodel.ext.android.sharedViewModel

class SearchFragment :
    Fragment(),
    SearchAdapter.OnUserClickListener {
    private var searchAdapter = SearchAdapter()

    private val viewModel by sharedViewModel<SearchViewModel>()

    private val statusObserver = Observer<PageDownloadingStatus> {
        when (it) {
            is PageLoading -> {
                Log.d("search_log", "Search=LOADING")
            }
            is PageSuccessful -> {
                Log.d("search_log", "Search=SUCCESSFUL")
            }
            is PageError -> {
                Log.d("search_log", "Search=ERROR")
            }
        }
    }

    private val usersObserver = Observer<PagedList<User>> {
        searchAdapter.submitList(it)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        searchModuleLoader.load()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_search, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        view.search_recycler_view.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = searchAdapter.apply { listener = this@SearchFragment }
        }

        search_edit_text.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {/*nothing to do*/
            }

            override fun beforeTextChanged(
                p0: CharSequence?,
                p1: Int,
                p2: Int,
                p3: Int
            ) {}

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                if (p0.toString().length > 0) {
                    search(p0.toString())
                }

            }

        })

    }

    override fun onUserClick(position: Int, view: View?) {
        Log.d("search_log", "нажали на $position элемент списка.")
    }

    private fun search(query: String) {
        viewModel.usersList?.removeObservers(viewLifecycleOwner)
        viewModel.status?.removeObservers(viewLifecycleOwner)

        viewModel.search(query)?.observe(viewLifecycleOwner, usersObserver)
        viewModel.status()?.observe(viewLifecycleOwner, statusObserver)
    }


    private fun setUpAdapter(){

    }
}
