package com.systems.magora.search

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import com.systems.magora.twitter.core.PageDownloadingStatus
import com.systems.magora.twitter.core.model.locale.User
import com.systems.magora.twitter.networking.RemoteRepositoryImpl
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import org.koin.core.KoinComponent

class SearchViewModel(val reppository: RemoteRepositoryImpl, application: Application) :
    AndroidViewModel(application), KoinComponent {
    var userId = 0L

    var usersList: LiveData<PagedList<User>>? = null
    var status: LiveData<PageDownloadingStatus>? = null

    private var viewModelJob = Job()
    private val viewModelScope = CoroutineScope(Dispatchers.Main + viewModelJob)
    private val paginationScope = CoroutineScope(Dispatchers.IO)

    private lateinit var factory: SearchDataSourceFactory

    fun status(): LiveData<PageDownloadingStatus>? {
        status = Transformations.switchMap(factory.liveData, SearchDataSource::status)
        return status
    }

    fun search(query: String): LiveData<PagedList<User>>? {
        factory = SearchDataSourceFactory(paginationScope, reppository, query)
        usersList = LivePagedListBuilder<Int, User>(factory, pagedListConfig()).build()
        return usersList
    }

    fun pagedListConfig() =
        PagedList.Config.Builder().setPageSize(40).setInitialLoadSizeHint(120).build()


}