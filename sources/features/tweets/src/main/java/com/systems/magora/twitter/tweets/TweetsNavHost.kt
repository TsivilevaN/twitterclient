package com.systems.magora.twitter.tweets

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.navArgs
import com.systems.magora.twitter.tweets.di.tweetsPagingModuleLoader
import com.systems.magora.twitter.tweets.pagination.PagingTweetsViewModel
import org.koin.androidx.viewmodel.ext.android.sharedViewModel

/**
 * A simple [Fragment] subclass.
 */
class TweetsNavHost : Fragment() {
    val viewodel by sharedViewModel<PagingTweetsViewModel>()
  //TODO раскоментить поле сборки
   private val args by navArgs<TweetsNavHostArgs>()
    private val userId by lazy {
        args.userId
    }
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
      return inflater.inflate(R.layout.fragment_tweets_nav_host, container, false)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        tweetsPagingModuleLoader.load()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //TODO раскоментить поле сборки
          viewodel.userId = userId
            Log.d(
                "profile_log",
                "TweetsNavHost.onViewCreated() args.userId=${userId}, View Model Object ${viewodel}"
            )
    }
}
