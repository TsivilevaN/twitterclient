package com.systems.magora.twitter.tweets.pagination

import android.view.View
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import androidx.navigation.NavController
import androidx.navigation.fragment.FragmentNavigator
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import com.systems.magora.twitter.core.PageDownloadingStatus
import com.systems.magora.twitter.core.model.locale.Tweet
import com.systems.magora.twitter.networking.repos.RemoteRepository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch

class PagingTweetsViewModel(
    private val tweetsDataSourceFactory: TweetsDataSourceFactory,
    private val repository: RemoteRepository,
    private val scope:CoroutineScope
) :
    ViewModel() {

    var userId:Long?=0
    var tweetsList: LiveData<PagedList<Tweet>>? = null
    var downloadngStatus: LiveData<PageDownloadingStatus>? = null
    lateinit var navController: NavController

    fun getStatus(): LiveData<PageDownloadingStatus>? {
        downloadngStatus = Transformations.switchMap<TweetsDataSource, PageDownloadingStatus>(
            tweetsDataSourceFactory.liveData,
            TweetsDataSource::status
        )
        return downloadngStatus
    }

    fun getTweets(): LiveData<PagedList<Tweet>>? {
        tweetsList = LivePagedListBuilder(tweetsDataSourceFactory, pagedListConfig()).build()
        return tweetsList
    }

    private fun pagedListConfig() = PagedList.Config.Builder()
        .setInitialLoadSizeHint(10)
        .setPageSize(10)
        .build()

    fun showProfile(userId: Long, view: View) {
        //TODO раскоментить поле сборки
     /*   navController.navigate(
            NavMainDirections.actionGlobalProfileNavHost(userId),
            FragmentNavigator.Extras.Builder()
                .addSharedElement(view, "userIdTN").build()
        )*/
    }

    fun addToFav(tweet:Tweet):LiveData<Boolean>{
        var likeLiveData=MutableLiveData<Boolean>()
            scope.launch {
                likeLiveData.postValue(repository.likeTweet(tweetId = tweet.tweetId))
            }
        return likeLiveData
    }

    fun removeFromFav(tweet: Tweet): LiveData<Boolean> {
        var dislikeLiveData =MutableLiveData<Boolean>()
        scope.launch {
            dislikeLiveData.postValue(repository.dislikeTweet(tweetId = tweet.tweetId))
        }
        return dislikeLiveData
    }
}