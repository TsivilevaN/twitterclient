package com.systems.magora.twitter.tweets.pagination

import androidx.lifecycle.MutableLiveData
import androidx.paging.ItemKeyedDataSource
import com.systems.magora.twitter.core.*
import com.systems.magora.twitter.core.model.locale.Tweet
import com.systems.magora.twitter.networking.repos.RemoteRepository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch

class TweetsDataSource(
    var repository: RemoteRepository,
    var scope: CoroutineScope
) : ItemKeyedDataSource<Long, Tweet>() {
    val status = MutableLiveData<PageDownloadingStatus>()
    override fun loadInitial(
        params: LoadInitialParams<Long>,
        callback: LoadInitialCallback<Tweet>
    ) {
        status.postValue(PageLoading)
        scope.launch {
            val networkResponse = repository.getTweets(count = params.requestedLoadSize)
            when (networkResponse) {
                is NetworkSuccessfull -> {
                    callback.onResult(networkResponse.data)
                }
                is NetworkError -> {
                    status.postValue(PageError)
                    when (networkResponse.cachedData) {
                        null -> callback.onResult(emptyList())
                        else -> callback.onResult(networkResponse.cachedData!!)
                    }
                }
            }
        }
    }

    override fun loadAfter(params: LoadParams<Long>, callback: LoadCallback<Tweet>) {
        status.postValue(PageLoading)
        scope.launch {
            val networkResponse = repository.getTweetsAfter(
                count = params.requestedLoadSize,
                maxId = params.key
            )
            when (networkResponse) {
                is NetworkSuccessfull -> {
                    status.postValue(PageSuccessful)
                    callback.onResult(networkResponse.data)
                }
                is NetworkError -> {
                    status.postValue(PageError)
                    when (networkResponse.cachedData) {
                        null -> callback.onResult(emptyList())
                        else -> callback.onResult(networkResponse.cachedData!!)
                    }
                }
            }
        }
    }

    override fun loadBefore(params: LoadParams<Long>, callback: LoadCallback<Tweet>) {
        status.postValue(PageLoading)
        scope.launch {
            val networkResponse = repository.getTweetsBefore(
                count = params.requestedLoadSize,
                startId = params.key
            )
            when (networkResponse) {
                is NetworkSuccessfull -> {
                    callback.onResult(networkResponse.data)
                }
                is NetworkError -> {
                    status.postValue(PageError)
                    when (networkResponse.cachedData) {
                        null -> callback.onResult(emptyList())
                        else -> callback.onResult(networkResponse.cachedData!!)
                    }
                }
            }
        }
    }

    override fun getKey(item: Tweet): Long {
        return item.tweetId
    }
}