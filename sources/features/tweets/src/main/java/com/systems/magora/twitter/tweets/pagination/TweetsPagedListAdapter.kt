package com.systems.magora.twitter.tweets.pagination

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.systems.magora.twitter.core.GlideApp
import com.systems.magora.twitter.core.model.locale.Tweet
import com.systems.magora.twitter.tweets.R
import kotlinx.android.synthetic.main.item_tweet.view.*

class TweetsPagedListAdapter() :
    PagedListAdapter<Tweet, TweetsPagedListAdapter.ViewHolder>(TweetsDiffUtilCallbaks()) {

    interface OnItemClickListener {
        fun onItemClick(position: Int, view: View?)
    }
    var listener: OnItemClickListener? = null

    override fun onCreateViewHolder( parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_tweet, parent, false)
        return ViewHolder(view,listener)
    }


    class ViewHolder(itemView: View, val listener: TweetsPagedListAdapter.OnItemClickListener?) :
        RecyclerView.ViewHolder(itemView),
        View.OnClickListener {
        val avatarCircleImageView = itemView.avatar_image_view
        val dateTextView = itemView.date_text_vew
        val nameTextView = itemView.name_text_view
        val screenNameTextView = itemView.short_name_text_view
        val mediaImageView = itemView.media_image_view
        val tweetTextView = itemView.tweet_text_view

        val favoriteCountTextView = itemView.fav_count_text_view
        val addToFavImageView = itemView.add_to_fav_image_view
        val retweetsCountTextView = itemView.retweets_count_text_view
        val retweetImageView = itemView.retweet_image_view

        init {
            avatarCircleImageView.setOnClickListener(this)
            addToFavImageView.setOnClickListener(this)
            retweetImageView.setOnClickListener(this)
            retweetsCountTextView.setOnClickListener(this)
            retweetImageView.setOnClickListener(this)
        }


        override fun onClick(p0: View?) {
            listener?.onItemClick(position = adapterPosition, view = p0)
        }

        fun bind(tweet: Tweet) {
            with(tweet) {
                GlideApp.with(avatarCircleImageView)
                    .load(userImageUrl)
                    .centerCrop()
                    .into(avatarCircleImageView)

                GlideApp.with(mediaImageView)
                    .asDrawable()
                    .load(mediaUrl)
                    .centerCrop()
                    .placeholder(R.drawable.loading)
                    .error(R.drawable.download_error)
                    .into(mediaImageView)

                tweetTextView.text = text
                dateTextView.text = dateString
                nameTextView.text = userName
                screenNameTextView.text = userScreenName
                retweetsCountTextView.text = retweetsCount.toString()
                favoriteCountTextView.text = favoriteCount.toString()

                if(this.isFavorie) addToFavImageView.setImageResource(R.drawable.heart_icon_full)
                else addToFavImageView.setImageResource(R.drawable.add_to_fav)


            }
        }

    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        getItem(position)?.let { holder.bind(it) }
    }

    fun setLike(position:Int){
        Log.d("tweets_log","  currentList?.get(position)?.isFavorie ${  currentList?.get(position)?.isFavorie}")
        currentList?.get(position)?.isFavorie=true
        currentList?.get(position)?.favoriteCount?.plus(1)
        notifyItemChanged(position)
    }

    fun removeLike(position:Int){
        currentList?.get(position)?.isFavorie=false
        currentList?.get(position)?.favoriteCount?.minus(1)
        notifyItemChanged(position)
    }
}