package com.systems.magora.twitter.tweets.di

import com.systems.magora.twitter.core.BaseModuleLoader
import com.systems.magora.twitter.tweets.TweetsFragment
import com.systems.magora.twitter.tweets.pagination.PagingTweetsViewModel
import com.systems.magora.twitter.tweets.pagination.TweetsDataSource
import com.systems.magora.twitter.tweets.pagination.TweetsDataSourceFactory
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val fragmentModule = module {
    factory { TweetsFragment() }
}

val viewModelModule = module {
    viewModel { PagingTweetsViewModel(get(), get(), CoroutineScope(Dispatchers.IO)) }
}

val dataSourceModule = module {
    factory { TweetsDataSource(get(), get()) }
    factory { TweetsDataSourceFactory(get()) }
    factory { CoroutineScope(Dispatchers.IO) }
}

internal object tweetsPagingModuleLoader : BaseModuleLoader(
    listOf(fragmentModule, viewModelModule, dataSourceModule)
)