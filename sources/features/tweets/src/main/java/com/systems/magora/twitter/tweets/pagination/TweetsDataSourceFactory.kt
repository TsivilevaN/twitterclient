package com.systems.magora.twitter.tweets.pagination

import androidx.lifecycle.MutableLiveData
import androidx.paging.DataSource
import com.systems.magora.twitter.core.model.locale.Tweet

class TweetsDataSourceFactory(private val tweetsDataSource: TweetsDataSource) :
    DataSource.Factory<Long, Tweet>() {
    val liveData = MutableLiveData<TweetsDataSource>()
    override fun create(): DataSource<Long, Tweet> {
        liveData.postValue(tweetsDataSource)
        return tweetsDataSource
    }
}