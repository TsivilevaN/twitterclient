package com.systems.magora.twitter.tweets

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.children
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.paging.PagedList
import androidx.recyclerview.widget.LinearLayoutManager
import com.systems.magora.twitter.core.*
import com.systems.magora.twitter.core.model.locale.Tweet
import com.systems.magora.twitter.tweets.pagination.PagingTweetsViewModel
import com.systems.magora.twitter.tweets.pagination.TweetsPagedListAdapter
import kotlinx.android.synthetic.main.fragment_tweets.view.*
import org.koin.androidx.viewmodel.ext.android.sharedViewModel


class TweetsFragment :
    Fragment(),
    TweetsPagedListAdapter.OnItemClickListener {

    private val viewodel by sharedViewModel<PagingTweetsViewModel>()
    private val adapter = TweetsPagedListAdapter()

    private val tweetsObserver = Observer<PagedList<Tweet>> {
        adapter.submitList(it)
    }

    private val statusObserver = Observer<PageDownloadingStatus> {
        when (it) {
            is PageSuccessful -> {
                Log.d("pagnation_log", "SUCCESSFUL")
                view?.error_linear_layout?.visibility = View.GONE
                view?.swiper?.isRefreshing = false
            }
            is PageError -> {
                Log.d("pagnation_log", "ERROR")
                view?.error_linear_layout?.visibility = View.VISIBLE
                view?.swiper?.isRefreshing = false

            }
            is PageLoading -> {
                Log.d("pagnation_log", "LOADING...")
                view?.swiper?.isRefreshing = true
            }
        }
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_tweets, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setUpAdapter()
        downloadTweets()
        viewodel.navController = getMainNavController()

        view.reload_button.setOnClickListener { downloadTweets() }

        view.swiper.setOnRefreshListener { downloadTweets() }

        view.tweets_toolbar.menu.children.forEach { _menuItem ->
            _menuItem.setOnMenuItemClickListener { it ->
                when (it.itemId) {
                    R.id.reload_menu_item -> downloadTweets()
                    R.id.cleare_cache_menu_item -> {
                    }
                }
                true
            }
        }
    }


    private fun setUpAdapter() {
        view?.tweets_recycler_view?.layoutManager = LinearLayoutManager(context)
        view?.tweets_recycler_view?.adapter = adapter
        adapter.listener = this@TweetsFragment

    }

    private fun downloadTweets() {
        view?.swiper?.isRefreshing = true
        /*re-observe, because PagedList is immutable */
        viewodel.tweetsList?.removeObservers(viewLifecycleOwner)
        viewodel.downloadngStatus?.removeObservers(viewLifecycleOwner)

        viewodel.getTweets()?.observe(viewLifecycleOwner, tweetsObserver)
        viewodel.getStatus()?.observe(viewLifecycleOwner, statusObserver)
    }

    override fun onItemClick(position: Int, view: View?) {
        Log.d(
            "tweets_log",
            "onItemClick() pressed on ${position}, element=${adapter.currentList?.get(position)}"
        )

        when (view?.id) {
            R.id.avatar_image_view -> {
                adapter.currentList?.get(position)?.authorId?.let {
                    viewodel.showProfile(
                        userId = it,
                        view = view
                    )
                }
            }
            R.id.add_to_fav_image_view -> {
                val selectedTweet = adapter.currentList?.get(position)

                selectedTweet?.let { _selectedTweet: Tweet ->
                    if (_selectedTweet.isFavorie) {
                        val dislikeLiveData = viewodel.removeFromFav(_selectedTweet)
                        dislikeLiveData.observe(
                            viewLifecycleOwner,
                            Observer { _networkSuccessful: Boolean ->
                                if (_networkSuccessful) adapter.removeLike(position)
                                dislikeLiveData.removeObservers(viewLifecycleOwner)
                            })
                    } else {
                        val likeLiveData = viewodel.addToFav(_selectedTweet)
                        likeLiveData.observe(
                            viewLifecycleOwner,
                            Observer { _networkSuccessful: Boolean ->
                                if (_networkSuccessful) adapter.setLike(position)
                                likeLiveData.removeObservers(viewLifecycleOwner)
                            })
                    }
                }
                adapter.currentList?.get(position)?.let {
                }
            }
        }
    }

}
