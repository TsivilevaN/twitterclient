package com.systems.magora.twitter.tweets.pagination

import androidx.recyclerview.widget.DiffUtil
import com.systems.magora.twitter.core.model.locale.Tweet

class TweetsDiffUtilCallbaks : DiffUtil.ItemCallback<Tweet>() {
    override fun areItemsTheSame(oldItem: Tweet, newItem: Tweet): Boolean {
        return oldItem.tweetId == newItem.tweetId
    }
    override fun areContentsTheSame(oldItem: Tweet, newItem: Tweet): Boolean {
        return oldItem.equals(newItem)
    }
}