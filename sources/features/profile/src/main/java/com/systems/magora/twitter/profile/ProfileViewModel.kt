package com.systems.magora.twitter.profile

import android.app.Application
import android.view.View
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.navigation.NavController
import androidx.navigation.fragment.FragmentNavigator
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import com.systems.magora.twitter.client.NavMainDirections
import com.systems.magora.twitter.core.NetworkResponse
import com.systems.magora.twitter.core.model.locale.AuthorizationData
import com.systems.magora.twitter.core.model.locale.User
import com.systems.magora.twitter.networking.RemoteRepositoryImpl
import kotlinx.coroutines.*
import org.koin.core.KoinComponent

class ProfileViewModel(val reppository: RemoteRepositoryImpl, app: Application) :
    AndroidViewModel(app), KoinComponent {
    var profileInfo = MutableLiveData<NetworkResponse<User>>()
    var profileUserId = 0L

    lateinit var navController: NavController
    private var viewModelJob = Job()
    private val viewModelScope = CoroutineScope(Dispatchers.Main + viewModelJob)

    fun getProfileInfo(id: Long): LiveData<NetworkResponse<User>> {
        viewModelScope.launch {
            profileInfo.postValue(reppository.getProfileInfo(id))
        }
        return profileInfo
    }

     fun signOut() {
        viewModelScope.launch {
            Firebase.auth.signOut()
            reppository.cleareCache(AuthorizationData::class)
        }
    }

    fun navigateToLogin() {
        navController.navigate(R.id.action_global_loginNavHost)
    }

    override fun onCleared() {
        super.onCleared()
        viewModelScope.coroutineContext.cancelChildren()
    }
}
