package com.systems.magora.twitter.profile

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.transition.TransitionInflater
import com.bumptech.glide.Glide
import com.systems.magora.twitter.core.BaseFragment
import com.systems.magora.twitter.core.NetworkError
import com.systems.magora.twitter.core.NetworkSuccessfull
import com.systems.magora.twitter.core.getMainNavController
import com.systems.magora.twitter.core.model.isEmpty
import com.systems.magora.twitter.core.model.locale.User
import kotlinx.android.synthetic.main.fragment_profile.*
import kotlinx.android.synthetic.main.fragment_profile.view.*
import org.koin.androidx.viewmodel.ext.android.sharedViewModel


class ProfileFragment :
    BaseFragment() {

    val viewodel by sharedViewModel<ProfileViewModel>()
    override var needShowBottomBar = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        sharedElementEnterTransition =
            TransitionInflater.from(context).inflateTransition(android.R.transition.move)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_profile, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        view.toolbar.menu.findItem(R.id.signOutMenuButton)
            .setOnMenuItemClickListener {
                viewodel.signOut()
                viewodel.navController=getMainNavController()
                viewodel.navigateToLogin()
                true
               }

        profile_swipe_refresh.setOnRefreshListener { updateInfo() }
        profile_swipe_refresh.isRefreshing = true
        updateInfo()
    }

    private fun updateInfo() {
        view?.profile_linear_layout?.visibility=View.GONE
        viewodel.profileInfo.removeObservers(viewLifecycleOwner)
        viewodel.getProfileInfo(viewodel.profileUserId).observe(viewLifecycleOwner, Observer {
            when (it) {
                is NetworkSuccessfull -> {
                    showProfileInfo(user = it.data)
                }
                is NetworkError -> {
                    Toast.makeText(context, it.error?.message, Toast.LENGTH_LONG).show()
                    it.cachedData?.let { _cachedUser -> showProfileInfo(_cachedUser) }
                }
            }
            profile_swipe_refresh.isRefreshing = false
        })
    }

    private fun showProfileInfo(user: User) {
        if (!user.isEmpty()) {
            view?.profile_linear_layout?.visibility=View.VISIBLE
            Glide.with(context!!).load(user.avatarUrl).into(avatar_image_view)
            Log.d("profile_log", user.toString())
            view?.name_text_view?.text = user.userName
            view?.short_name_text_view?.text = user.screenName
            view?.joined_text_view?.text = user.joined
            view?.location_text_view?.text = user.location
            view?.website_text_view?.text = user.website
            view?.status_text_view?.text = user.status
            view?.followers_text_view?.text = user.followersCount.toString()
            view?.friend_text_view?.text = user.friendsCount.toString()
            view?.bio_text_view?.text = user.bio
        }
    }
}
