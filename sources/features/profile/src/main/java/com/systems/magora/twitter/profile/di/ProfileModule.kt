package com.systems.magora.twitter.profile.di

import com.systems.magora.twitter.core.BaseModuleLoader
import com.systems.magora.twitter.profile.ProfileViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val profileUseCase=module{
    viewModel { ProfileViewModel(reppository=get(),app=get()) }
}

internal object profileModuleLoader: BaseModuleLoader(listOf(profileUseCase))