package com.systems.magora.twitter.profile

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.navArgs
import com.systems.magora.twitter.profile.di.profileModuleLoader
import org.koin.androidx.viewmodel.ext.android.sharedViewModel

class ProfileNavHost : Fragment() {
    private val profileViewModel by sharedViewModel<ProfileViewModel>()

    //TODO раскоментить поле сборки
  private val args by navArgs<ProfileNavHostArgs>()
    private val userId by lazy {
        args.userId
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        profileModuleLoader.load()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //TODO раскоментить поле сборки
      profileViewModel.profileUserId = userId
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_profile_nav_host, container, false)
    }

}
