package com.systems.magora.twitter.login

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.firebase.auth.AuthResult
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import com.systems.magora.twitter.core.BaseActivity
import com.systems.magora.twitter.core.BaseFragment
import com.systems.magora.twitter.core.getCurrentUserId
import com.systems.magora.twitter.core.getMainNavController
import com.systems.magora.twitter.login.di.loginModuleLoader
import kotlinx.android.synthetic.main.fragment_login.*
import kotlinx.android.synthetic.main.fragment_login.view.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class LoginFragment :
    BaseFragment() {
    private val loginViewModel by viewModel<LoginViewModel>()
    override var needShowBottomBar: Boolean = false

    override fun onAttach(context: Context) {
        super.onAttach(context)
        loginModuleLoader.load()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_login, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        view.loginButton.setOnClickListener {
            (activity as? BaseActivity)?.let {

                val helper = object : AuthorizationHelper() {
                    override fun doOnSuccess(result: AuthResult) {
                        loginViewModel.saveAuthorizationData(result)
                        loginViewModel.navController = getMainNavController()

                        Firebase.auth.getCurrentUserId()?.let { it1 ->
                            loginViewModel.navigateToProfile( view.loginInfoTextView, it1 )
                        }
                    }

                    override fun doOnFailure(e: java.lang.Exception) {
                        loginInfoTextView.visibility = View.VISIBLE
                        loginInfoTextView.setText(R.string.login_failed_text)
                    }
                }

                helper.signIn(it)
            }
        }
    }

}







