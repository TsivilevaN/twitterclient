package com.systems.magora.twitter.login


import android.app.Application
import android.view.View
import androidx.lifecycle.AndroidViewModel
import androidx.navigation.NavController
import androidx.navigation.fragment.FragmentNavigator
import com.google.firebase.auth.AuthResult
import com.google.firebase.auth.OAuthCredential
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import com.systems.magora.twitter.client.NavMainDirections
import com.systems.magora.twitter.core.getCurrentTwitterUser
import com.systems.magora.twitter.core.getCurrentUserId
import com.systems.magora.twitter.core.model.locale.AuthorizationData
import com.systems.magora.twitter.networking.RemoteRepositoryImpl
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import org.koin.core.KoinComponent

class LoginViewModel(
    private val app: Application,
    private val repository: RemoteRepositoryImpl
) : AndroidViewModel(app),
    KoinComponent {

    lateinit var navController: NavController
    private var viewModelJob = Job()
    private val viewModelScope = CoroutineScope(Dispatchers.Main + viewModelJob)

    fun saveAuthorizationData(result: AuthResult): AuthorizationData {
        val authorizationData = getCredentials(result)
        authorizationData.userId?.let {
            viewModelScope.launch {
                repository.saveAuthorityInfo(authorizationData)
            }
        }
        return authorizationData
    }

    private fun getCredentials(result: AuthResult): AuthorizationData {
        val credentials = result.credential as? OAuthCredential
        val id = Firebase.auth.getCurrentUserId()
        return AuthorizationData(
            userId = id,
            access_token = credentials?.accessToken,
            access_secret_token = credentials?.secret
        )
    }

    fun navigateToProfile(view: View, userId: Long) {
        //TODO раскоментить поле сборки
        navController.navigate(
            NavMainDirections.actionGlobalProfileNavHost(userId),
            FragmentNavigator.Extras.Builder()
                .addSharedElement(view, "userIdTN").build()
        )
    }

    fun navigateToLogin() {
        navController.navigate(R.id.action_splashFragment_to_loginFragment)
    }
}