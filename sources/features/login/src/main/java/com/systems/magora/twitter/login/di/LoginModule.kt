package com.systems.magora.twitter.login.di

import com.systems.magora.twitter.core.BaseModuleLoader
import com.systems.magora.twitter.login.LoginViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val loginUseCase= module{
    viewModel { LoginViewModel(get(),get()) }
}

internal object loginModuleLoader: BaseModuleLoader(listOf(loginUseCase))