package com.systems.magora.twitter.login

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import com.systems.magora.twitter.core.getCurrentUserId
import com.systems.magora.twitter.core.getMainNavController
import com.systems.magora.twitter.login.di.loginModuleLoader
import kotlinx.android.synthetic.main.fragment_welcome.view.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class WelcomeFragment : Fragment() {
    private val loginViewModel by viewModel<LoginViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_welcome, container, false)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        loginModuleLoader.load()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (Firebase.auth.currentUser == null) {
            loginViewModel.navController = findNavController()
            loginViewModel.navigateToLogin()
        } else {
            loginViewModel.navController = getMainNavController()
            loginViewModel.navigateToProfile(
                view.animationLottieView,
                Firebase.auth.getCurrentUserId()!!
            )
        }
    }
}
