package com.systems.magora.twitter.login

import com.google.android.gms.tasks.Task
import com.google.firebase.auth.AuthResult
import com.google.firebase.auth.OAuthProvider
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import com.systems.magora.twitter.core.BaseActivity
import com.systems.magora.twitter.core.getCurrentUserId
import java.lang.Exception

abstract class AuthorizationHelper {
    abstract fun doOnSuccess(result:AuthResult)
    abstract fun doOnFailure(e:Exception)

    fun signIn(activity: BaseActivity) {
        val task = Firebase.auth.pendingAuthResult
        if (task == null) {
            startSignIn(activity)
        } else {
            finishSignIn(task)
        }
    }

    private fun startSignIn(activity: BaseActivity) {
        val provider = OAuthProvider.newBuilder("twitter.com").build()
        Firebase.auth
            .startActivityForSignInWithProvider(activity, provider)
            .addOnSuccessListener { doOnSuccess(it) }
            .addOnFailureListener { doOnFailure(it) }
    }

    private fun finishSignIn(task: Task<AuthResult>) {
        task.addOnSuccessListener { doOnSuccess(it) }
            .addOnFailureListener { doOnFailure(it) }
    }
}