package com.systems.magora.twitter.core

sealed class NetworkResponse<T>()
class NetworkSuccessfull <T> (val data:T) :NetworkResponse<T>()
class NetworkError<T>(val error:Exception?, val cachedData:T?) : NetworkResponse<T>()

sealed class PageDownloadingStatus
object PageLoading: PageDownloadingStatus()
object PageSuccessful: PageDownloadingStatus()
object PageError: PageDownloadingStatus()

