package com.systems.magora.twitter.core.model

import com.systems.magora.twitter.core.model.locale.AuthorizationData
import com.systems.magora.twitter.core.model.locale.Tweet
import com.systems.magora.twitter.core.model.locale.User
import com.systems.magora.twitter.core.model.remote.ProfilePojo
import com.systems.magora.twitter.core.model.remote.TweetsPojo
import com.twitter.sdk.android.core.TwitterAuthConfig

fun AuthorizationData.toTwitterAuthority() =
    TwitterAuthConfig(this.consumer_key, this.consumer_secret_token)


fun ProfilePojo.toUser(): User {
    val urls = this.entities.url
    val url: String =
        if (urls.urls.count() > 0) urls.urls[0].expandedUrl
        else ""

    return User(
        userId = this.id,
        userName = this.name,
        screenName = this.screenName,
        status = this.status.text,
        avatarUrl = this.profileImageUrl.replace("http", "https").replace("_normal", ""),
        avatarSmallUrl = this.profileImageUrl,
        website = url,
        joined = this.createdAt,
        location = this.location,
        bio = this.description,
        followersCount = this.followersCount,
        friendsCount = this.friendsCount,
        followed = this.following
    )
}

fun List<ProfilePojo>.toUserList(): List<User> {
    val list = mutableListOf<User>()
    this.forEach {
        list.add(it.toUser())
    }
    return list
}


fun TweetsPojo.toTweet(): Tweet {
    var mediaUrl = ""
    val medias = this.extendedEntities?.media
    /*если твит состоит только из ретвита */
    if (this.isQuoteStatus) {
/*то достать данные именно из ретвита - https://developer.twitter.com/en/docs/tweets/post-and-engage/api-reference/get-statuses-show-id*/
    } else {
        /*иначе, если твит содержит какую-то картинку*/
        if (!medias.isNullOrEmpty()) {
            mediaUrl = medias[0].mediaUrlHttps
        }
    }
    val date = this.createdAt.convertFromNetwork()
    val dateStr = date.toLocalDate()

    return Tweet(
        tweetId = this.id,
        authorId = this.user.id,
        userName = this.user.name,
        userScreenName = this.user.screenName,
        userImageUrl = this.user.profileImageUrl.replace("http", "https"),
        mediaUrl = mediaUrl,
        date = date,
        dateString = dateStr,
        text = this.text,
        isRetweeted = this.retweeted,
        isFavorie = this.favorited,
        retweetsCount = retweetCount,
        favoriteCount = this.favoriteCount
    )
}

fun List<TweetsPojo>.toTweets(): List<Tweet> {
    val list = mutableListOf<Tweet>()
    this.forEach {
        list.add(it.toTweet())
    }
    return list
}









