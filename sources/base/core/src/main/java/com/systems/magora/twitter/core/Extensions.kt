package com.systems.magora.twitter.core

import com.google.firebase.auth.AuthResult
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import com.systems.magora.twitter.core.model.locale.User

fun FirebaseAuth.getCurrentUserId() = this.currentUser
    ?.providerData
    ?.find { it.providerId == "twitter.com" }
    ?.uid?.toLong()

