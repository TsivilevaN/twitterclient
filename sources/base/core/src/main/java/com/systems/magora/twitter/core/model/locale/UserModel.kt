package com.systems.magora.twitter.core.model.locale

import androidx.room.*
import java.util.*

@Entity(tableName = "User", indices = [Index(value = ["userId"], unique = true)])
data class User(
    @PrimaryKey(autoGenerate = false)
    @ColumnInfo(name = "userId")
    var userId: Long = 0L,
    var avatarUrl: String = "",
    var avatarSmallUrl: String = "",
    var userName: String = "",
    var screenName: String = "",
    var status: String = "",
    var website: String = "",
    var joined: String = "",
    var location: String = "",
    var bio: String = "",
    var followersCount: Long = 0L,
    var friendsCount: Long = 0L,
    var followed:Boolean=false,
    var isMe:Boolean=false
)

@Entity(
    tableName = "Settings", indices = [Index(value = ["settingsId"], unique = true),
        Index(value = ["userId"], unique = true)]
)
data class Settings(
    @PrimaryKey(autoGenerate = false)
    @ColumnInfo(name = "settingsId")
    var settingsId: Long = 0L,

    @ForeignKey(
        entity = User::class,
        childColumns = ["userId"],
        parentColumns = ["userId"],
        onDelete = ForeignKey.CASCADE
    )
    var userId: Long = 0L
)


@Entity(tableName = "Tweets", indices = [Index(value = ["tweetId"], unique = true)])
data class Tweet(
    @PrimaryKey
    @ColumnInfo(name = "tweetId")
    var tweetId: Long = 0L,
    @ForeignKey(
        entity = User::class,
        childColumns = ["userId"],
        parentColumns = ["userId"],
        onDelete = ForeignKey.CASCADE,
        onUpdate = ForeignKey.CASCADE
    )
    var authorId:Long=0L,/*id автора поста*/
    var userId: Long = 0L/*id пользователя, в ленте которого находится данный пост*/,
    var userImageUrl: String = "",
    var userName: String = "",
    var userScreenName: String = "",
    var date:Date= Date(0L),
    var dateString: String = "",
    var mediaUrl: String = "",
    var isRetweeted: Boolean = false,
    var isFavorie: Boolean = false,
    var retweetsCount: Long = 0L,
    var favoriteCount: Long = 0L,
    var text: String = ""
)

@Entity
data class Followers(
    @PrimaryKey
    @ColumnInfo(name = "followerId")
    var followerId: Long = 0L,
    @ForeignKey(
        entity = User::class,
        childColumns = ["userId"],
        parentColumns = ["userId"],
        onDelete = ForeignKey.CASCADE,
        onUpdate = ForeignKey.CASCADE
    )
    var userId: Long = 0L
)

@Entity
data class Friends(
    @PrimaryKey
    @ColumnInfo(name = "friendId")
    var friendId: Long = 0L,
    @ForeignKey(
        entity = User::class,
        childColumns = ["userId"],
        parentColumns = ["userId"],
        onDelete = ForeignKey.CASCADE,
        onUpdate = ForeignKey.CASCADE
    )
    var userId: Long = 0L
)

@Entity(tableName = "Authorization", indices = [Index(value = ["userId"], unique = true)])
data class AuthorizationData(
    var userId: Long? = null,
    @PrimaryKey
    @ColumnInfo(name = "consumer_key")
    val consumer_key: String = "FHAGOsnVgJXwqaSnY3O5cCWIt",
    val consumer_secret_token: String = "DPo0KbG4WUgrQrNqkh1lgMSIeGPGs8KUoad7NlZpXndooA1I23",
    var access_token: String? = null,
    var access_secret_token: String? = null
) {

}

class DateConverter(){
    @TypeConverter
    fun fromDate(date:Date)=date.time
    @TypeConverter
    fun toDate(time:Long)=Date(time)
}

