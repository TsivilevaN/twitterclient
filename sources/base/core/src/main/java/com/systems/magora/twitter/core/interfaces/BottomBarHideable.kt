package com.systems.magora.twitter.core.interfaces

interface BottomBarHideable {
    fun hide()
    fun show()
}