package com.systems.magora.twitter.core

import com.bumptech.glide.annotation.GlideModule
import com.bumptech.glide.module.AppGlideModule



@GlideModule
class GlideDownloader: AppGlideModule()// new since Glide v4
