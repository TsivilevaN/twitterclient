package com.systems.magora.twitter.core

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import com.systems.magora.twitter.core.interfaces.BottomBarHideable

abstract class BaseFragment : Fragment() {
    open var needShowBottomBar = false

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val fragmentActivity = activity
        if (fragmentActivity != null && fragmentActivity is BottomBarHideable) {
            with(activity as BottomBarHideable) {
                if (needShowBottomBar) show()
                else hide()
            }
        }
    }

}