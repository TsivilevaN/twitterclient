package com.systems.magora.twitter.core.model.remote

import com.google.gson.annotations.SerializedName
import com.systems.magora.twitter.core.model.remote.ProfilePojo.Entities
/*import com.systems.magora.twitter.core.model.remote.ProfilePojo.Status.Entities_*/


class TweetsPojo {
    @SerializedName("created_at")
    var createdAt: String  = ""

    @SerializedName("id")
    var id: Long=0L
    @SerializedName("id_str")

    var idStr: String  = ""
    @SerializedName("text")

    var text: String  = ""
    @SerializedName("truncated")

    var truncated: Boolean = false 
    @SerializedName("entities")

    var entities: Entities? = null
    @SerializedName("extended_entities")

    var extendedEntities: ExtendedEntities? = null
    @SerializedName("source")

    var source: String  = ""
    @SerializedName("in_reply_to_status_id")

    var inReplyToStatusId: Any? = null
    @SerializedName("in_reply_to_status_id_str")

    var inReplyToStatusIdStr: Any? = null
    @SerializedName("in_reply_to_user_id")

    var inReplyToUserId: Any? = null
    @SerializedName("in_reply_to_user_id_str")

    var inReplyToUserIdStr: Any? = null
    @SerializedName("in_reply_to_screen_name")

    var inReplyToScreenName: Any? = null
    @SerializedName("user")

    var user: User = User()
    @SerializedName("geo")

    var geo: Any? = null
    @SerializedName("coordinates")

    var coordinates: Any? = null
    @SerializedName("place")

    var place: Any? = null
    @SerializedName("contributors")

    var contributors: Any? = null
    @SerializedName("retweeted_status")

    var retweetedStatus: RetweetedStatus? = null

    @SerializedName("is_quote_status")
    var isQuoteStatus: Boolean = false 
    @SerializedName("retweet_count")

    var retweetCount: Long=0L
    @SerializedName("favorite_count")

    var favoriteCount: Long=0L
    @SerializedName("favorited")

    var favorited: Boolean = false 
    @SerializedName("retweeted")

    var retweeted: Boolean = false 
    @SerializedName("possibly_sensitive")

    var possiblySensitive: Boolean = false 
    @SerializedName("possibly_sensitive_appealable")

    var possiblySensitiveAppealable: Boolean = false 
    @SerializedName("lang")

    var lang: String  = ""


    class RetweetedStatus {
        @SerializedName("created_at")

        var createdAt: String  = ""
        @SerializedName("id")

        var id: Long=0L
        @SerializedName("id_str")

        var idStr: String  = ""
        @SerializedName("text")

        var text: String  = ""
        @SerializedName("truncated")

        var truncated: Boolean = false 
        @SerializedName("entities")

        var entities: Entities__? = null
        @SerializedName("extended_entities")

        var extendedEntities: ExtendedEntities_? = null
        @SerializedName("source")

        var source: String  = ""
        @SerializedName("in_reply_to_status_id")

        var inReplyToStatusId: Any? = null
        @SerializedName("in_reply_to_status_id_str")

        var inReplyToStatusIdStr: Any? = null
        @SerializedName("in_reply_to_user_id")

        var inReplyToUserId: Any? = null
        @SerializedName("in_reply_to_user_id_str")

        var inReplyToUserIdStr: Any? = null
        @SerializedName("in_reply_to_screen_name")

        var inReplyToScreenName: Any? = null
        @SerializedName("user")

        var user: User_? = null
        @SerializedName("geo")

        var geo: Any? = null
        @SerializedName("coordinates")

        var coordinates: Any? = null
        @SerializedName("place")

        var place: Any? = null
        @SerializedName("contributors")

        var contributors: Any? = null
        @SerializedName("is_quote_status")

        var isQuoteStatus: Boolean = false 
        @SerializedName("retweet_count")

        var retweetCount: Long=0L
        @SerializedName("favorite_count")

        var favoriteCount: Long=0L
        @SerializedName("favorited")

        var favorited: Boolean = false 
        @SerializedName("retweeted")

        var retweeted: Boolean = false 
        @SerializedName("possibly_sensitive")

        var possiblySensitive: Boolean = false 
        @SerializedName("possibly_sensitive_appealable")

        var possiblySensitiveAppealable: Boolean = false 
        @SerializedName("lang")

        var lang: String  = ""

        class Entities__ {
            @SerializedName("hashtags")

            var hashtags: List<Any>? = null
            @SerializedName("symbols")

            var symbols: List<Any>? = null
            @SerializedName("user_mentions")

            var userMentions: List<Any>? = null
            @SerializedName("urls")

            var urls: List<Url__>? = null
            @SerializedName("media")

            var media: List<Medium____>? = null


            class Url__ {
                @SerializedName("url")

                var url: String  = ""
                @SerializedName("expanded_url")

                var expandedUrl: String  = ""
                @SerializedName("display_url")

                var displayUrl: String  = ""
                @SerializedName("indices")

                var indices: List<Int>? = null
            }


            class Medium____ {
                @SerializedName("id")

                var id: Long=0L
                @SerializedName("id_str")

                var idStr: String  = ""
                @SerializedName("indices")

                var indices: List<Int>? = null
                @SerializedName("media_url")

                var mediaUrl: String  = ""
                @SerializedName("media_url_https")

                var mediaUrlHttps: String  = ""
                @SerializedName("url")

                var url: String  = ""
                @SerializedName("display_url")

                var displayUrl: String  = ""
                @SerializedName("expanded_url")

                var expandedUrl: String  = ""
                @SerializedName("type")

                var type: String  = ""
                @SerializedName("sizes")

                var sizes: Sizes__? = null
            }


            class Sizes__ {
                @SerializedName("thumb")

                var thumb: Thumb__? = null
                @SerializedName("medium")

                var medium: Medium_____? = null
                @SerializedName("small")

                var small: Small__? = null
                @SerializedName("large")

                var large: Large__? = null

            }

            class Thumb__ {
                @SerializedName("w")

                var w: Long=0L
                @SerializedName("h")

                var h: Long=0L
                @SerializedName("resize")

                var resize: String  = ""
            }

            class Medium_____ {
                @SerializedName("w")

                var w: Long=0L
                @SerializedName("h")

                var h: Long=0L
                @SerializedName("resize")

                var resize: String  = ""
            }


            class Small__ {
                @SerializedName("w")

                var w: Long=0L
                @SerializedName("h")

                var h: Long=0L
                @SerializedName("resize")

                var resize: String  = ""
            }

            class Large__ {
                @SerializedName("w")

                var w: Long=0L
                @SerializedName("h")

                var h: Long=0L
                @SerializedName("resize")

                var resize: String  = ""
            }

        }

        class ExtendedEntities_ {
            @SerializedName("media")

            var media: List<Medium______>? = null

            class Medium______ {
                @SerializedName("id")

                var id: Long=0L
                @SerializedName("id_str")

                var idStr: String  = ""
                @SerializedName("indices")

                var indices: List<Int>? = null
                @SerializedName("media_url")

                var mediaUrl: String  = ""
                @SerializedName("media_url_https")

                var mediaUrlHttps: String  = ""
                @SerializedName("url")

                var url: String  = ""
                @SerializedName("display_url")

                var displayUrl: String  = ""
                @SerializedName("expanded_url")

                var expandedUrl: String  = ""
                @SerializedName("type")

                var type: String  = ""
                @SerializedName("sizes")

                var sizes: Sizes___? = null


                class Sizes___ {
                    @SerializedName("thumb")

                    var thumb: Thumb___? = null
                    @SerializedName("medium")

                    var medium: Medium_______? = null
                    @SerializedName("small")

                    var small: Small___? = null
                    @SerializedName("large")

                    var large: Large___? = null


                    class Thumb___ {
                        @SerializedName("w")

                        var w: Long=0L
                        @SerializedName("h")

                        var h: Long=0L
                        @SerializedName("resize")

                        var resize: String  = ""
                    }

                    class Medium_______ {
                        @SerializedName("w")

                        var w: Long=0L
                        @SerializedName("h")

                        var h: Long=0L
                        @SerializedName("resize")

                        var resize: String  = ""
                    }

                    class Small___ {
                        @SerializedName("w")

                        var w: Long=0L
                        @SerializedName("h")

                        var h: Long=0L
                        @SerializedName("resize")

                        var resize: String  = ""
                    }

                    class Large___ {
                        @SerializedName("w")

                        var w: Long=0L
                        @SerializedName("h")

                        var h: Long=0L
                        @SerializedName("resize")

                        var resize: String  = ""
                    }

                }


            }


        }


        class User_ {
            @SerializedName("id")

            var id: Long=0L
            @SerializedName("id_str")

            var idStr: String  = ""
            @SerializedName("name")

            var name: String  = ""
            @SerializedName("screen_name")

            var screenName: String  = ""
            @SerializedName("location")

            var location: String  = ""
            @SerializedName("description")

            var description: String  = ""
            @SerializedName("url")

            var url: String  = ""
            @SerializedName("entities")

            var entities: Entities___? = null
            @SerializedName("protected")

            var _protected: Boolean = false 
            @SerializedName("followers_count")

            var followersCount: Long=0L
            @SerializedName("friends_count")

            var friendsCount: Long=0L
            @SerializedName("listed_count")

            var listedCount: Long=0L
            @SerializedName("created_at")

            var createdAt: String  = ""
            @SerializedName("favourites_count")

            var favouritesCount: Long=0L
            @SerializedName("utc_offset")

            var utcOffset: Any? = null
            @SerializedName("time_zone")

            var timeZone: Any? = null
            @SerializedName("geo_enabled")

            var geoEnabled: Boolean = false 
            @SerializedName("verified")

            var verified: Boolean = false 
            @SerializedName("statuses_count")

            var statusesCount: Long=0L
            @SerializedName("lang")

            var lang: Any? = null
            @SerializedName("contributors_enabled")

            var contributorsEnabled: Boolean = false 
            @SerializedName("is_translator")

            var isTranslator: Boolean = false 
            @SerializedName("is_translation_enabled")

            var isTranslationEnabled: Boolean = false 
            @SerializedName("profile_background_color")

            var profileBackgroundColor: String  = ""
            @SerializedName("profile_background_image_url")

            var profileBackgroundImageUrl: String  = ""
            @SerializedName("profile_background_image_url_https")

            var profileBackgroundImageUrlHttps: String  = ""
            @SerializedName("profile_background_tile")

            var profileBackgroundTile: Boolean = false 
            @SerializedName("profile_image_url")

            var profileImageUrl: String  = ""
            @SerializedName("profile_image_url_https")

            var profileImageUrlHttps: String  = ""
            @SerializedName("profile_banner_url")

            var profileBannerUrl: String  = ""
            @SerializedName("profile_link_color")

            var profileLinkColor: String  = ""
            @SerializedName("profile_sidebar_border_color")

            var profileSidebarBorderColor: String  = ""
            @SerializedName("profile_sidebar_fill_color")

            var profileSidebarFillColor: String  = ""
            @SerializedName("profile_text_color")

            var profileTextColor: String  = ""
            @SerializedName("profile_use_background_image")

            var profileUseBackgroundImage: Boolean = false 
            @SerializedName("has_extended_profile")

            var hasExtendedProfile: Boolean = false 
            @SerializedName("default_profile")

            var defaultProfile: Boolean = false 
            @SerializedName("default_profile_image")

            var defaultProfileImage: Boolean = false 
            @SerializedName("following")

            var following: Boolean = false 
            @SerializedName("follow_request_sent")

            var followRequestSent: Boolean = false 
            @SerializedName("notifications")

            var notifications: Boolean = false 
            @SerializedName("translator_type")

            var translatorType: String  = ""


            class Entities___ {
                @SerializedName("url")

                var url: Url___? = null
                @SerializedName("description")

                var description: Description_? = null


                class Url___ {
                    @SerializedName("urls")

                    var urls: List<Url____>? = null

                    class Url____ {
                        @SerializedName("url")

                        var url: String  = ""
                        @SerializedName("expanded_url")

                        var expandedUrl: String  = ""
                        @SerializedName("display_url")

                        var displayUrl: String  = ""
                        @SerializedName("indices")

                        var indices: List<Int>? = null
                    }

                }

                class Description_ {
                    @SerializedName("urls")

                    var urls: List<Any>? = null
                }

            }

        }

    }

    class ExtendedEntities {
        @SerializedName("media")

        var media: List<Medium__>? = null

        class Medium__ {
            @SerializedName("id")

            var id: Long=0L
            @SerializedName("id_str")

            var idStr: String  = ""
            @SerializedName("indices")

            var indices: List<Int>? = null
            @SerializedName("media_url")

            var mediaUrl: String  = ""
            @SerializedName("media_url_https")

            var mediaUrlHttps: String  = ""
            @SerializedName("url")

            var url: String  = ""
            @SerializedName("display_url")

            var displayUrl: String  = ""
            @SerializedName("expanded_url")

            var expandedUrl: String  = ""
            @SerializedName("type")

            var type: String  = ""
            @SerializedName("sizes")

            var sizes: Sizes_? = null
            @SerializedName("source_status_id")

            var sourceStatusId: Long=0L
            @SerializedName("source_status_id_str")

            var sourceStatusIdStr: String  = ""
            @SerializedName("source_user_id")

            var sourceUserId: Long=0L
            @SerializedName("source_user_id_str")

            var sourceUserIdStr: String  = ""


            class Sizes_ {
                @SerializedName("thumb")

                var thumb: Thumb_? = null
                @SerializedName("medium")

                var medium: Medium___? = null
                @SerializedName("small")

                var small: Small_? = null
                @SerializedName("large")

                var large: Large_? = null
            }


            class Medium___ {
                @SerializedName("w")

                var w: Long=0L
                @SerializedName("h")

                var h: Long=0L
                @SerializedName("resize")

                var resize: String  = ""
            }


            class Thumb_ {
                @SerializedName("w")

                var w: Long=0L
                @SerializedName("h")

                var h: Long=0L
                @SerializedName("resize")

                var resize: String  = ""
            }


            class Small_ {
                @SerializedName("w")

                var w: Long=0L
                @SerializedName("h")

                var h: Long=0L
                @SerializedName("resize")

                var resize: String  = ""
            }

            class Large_ {
                @SerializedName("w")

                var w: Long=0L
                @SerializedName("h")

                var h: Long=0L
                @SerializedName("resize")

                var resize: String  = ""
            }

        }


    }

    class User {
        @SerializedName("id")

        var id: Long=0L
        @SerializedName("id_str")

        var idStr: String  = ""
        @SerializedName("name")

        var name: String  = ""
        @SerializedName("screen_name")

        var screenName: String  = ""
        @SerializedName("location")

        var location: String  = ""
        @SerializedName("description")

        var description: String  = ""
        @SerializedName("url")

        var url: Any? = null
      /*  @SerializedName("entities")

        var entities: Entities_? = null*/
        @SerializedName("protected")

        var _protected: Boolean = false 
        @SerializedName("followers_count")

        var followersCount: Long=0L
        @SerializedName("friends_count")

        var friendsCount: Long=0L
        @SerializedName("listed_count")

        var listedCount: Long=0L
        @SerializedName("created_at")

        var createdAt: String  = ""
        @SerializedName("favourites_count")

        var favouritesCount: Long=0L
        @SerializedName("utc_offset")

        var utcOffset: Any? = null
        @SerializedName("time_zone")

        var timeZone: Any? = null
        @SerializedName("geo_enabled")

        var geoEnabled: Boolean = false 
        @SerializedName("verified")

        var verified: Boolean = false 
        @SerializedName("statuses_count")

        var statusesCount: Long=0L
        @SerializedName("lang")

        var lang: Any? = null
        @SerializedName("contributors_enabled")

        var contributorsEnabled: Boolean = false 
        @SerializedName("is_translator")

        var isTranslator: Boolean = false 
        @SerializedName("is_translation_enabled")

        var isTranslationEnabled: Boolean = false 
        @SerializedName("profile_background_color")

        var profileBackgroundColor: String  = ""
        @SerializedName("profile_background_image_url")

        var profileBackgroundImageUrl: String  = ""
        @SerializedName("profile_background_image_url_https")

        var profileBackgroundImageUrlHttps: String  = ""
        @SerializedName("profile_background_tile")

        var profileBackgroundTile: Boolean = false 
        @SerializedName("profile_image_url")

        var profileImageUrl: String  = ""
        @SerializedName("profile_image_url_https")

        var profileImageUrlHttps: String  = ""
        @SerializedName("profile_banner_url")

        var profileBannerUrl: String  = ""
        @SerializedName("profile_link_color")

        var profileLinkColor: String  = ""
        @SerializedName("profile_sidebar_border_color")

        var profileSidebarBorderColor: String  = ""
        @SerializedName("profile_sidebar_fill_color")

        var profileSidebarFillColor: String  = ""
        @SerializedName("profile_text_color")

        var profileTextColor: String  = ""
        @SerializedName("profile_use_background_image")

        var profileUseBackgroundImage: Boolean = false 
        @SerializedName("has_extended_profile")

        var hasExtendedProfile: Boolean = false 
        @SerializedName("default_profile")

        var defaultProfile: Boolean = false 
        @SerializedName("default_profile_image")

        var defaultProfileImage: Boolean = false 
        @SerializedName("following")

        var following: Boolean = false 
        @SerializedName("follow_request_sent")

        var followRequestSent: Boolean = false 
        @SerializedName("notifications")

        var notifications: Boolean = false 
        @SerializedName("translator_type")

        var translatorType: String  = ""
    }

}
