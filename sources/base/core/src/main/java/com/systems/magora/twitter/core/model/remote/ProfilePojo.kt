package com.systems.magora.twitter.core.model.remote
import com.google.gson.annotations.SerializedName

data class ProfilePojo(
    @SerializedName("id")
    var id: Long = 0L,
    @SerializedName("id_str")
    var idStr: String = "",
    @SerializedName("name")
    var name: String = "",
    @SerializedName("screen_name")
    var screenName: String = "",
    @SerializedName("location")
    var location: String = "",
    @SerializedName("description")
    var description: String = "",
    @SerializedName("url")
    var url: String = "",
    @SerializedName("entities")
    var entities: Entities = Entities(),
    @SerializedName("protected")
    var protected: Boolean? = null,
    @SerializedName("followers_count")
    var followersCount: Long = 0L,
    @SerializedName("friends_count")
    var friendsCount: Long = 0L,
    @SerializedName("listed_count")
    var listedCount: Long = 0L,
    @SerializedName("created_at")
    var createdAt: String = "",
    @SerializedName("favourites_count")
    var favouritesCount: Long = 0L,
    @SerializedName("utc_offset")
    var utcOffset: String = "",
    @SerializedName("time_zone")
    var timeZone: String = "",
    @SerializedName("geo_enabled")
    var geoEnabled: Boolean? = null,
    @SerializedName("verified")
    var verified: Boolean? = null,
    @SerializedName("statuses_count")
    var statusesCount: Long = 0L,
    @SerializedName("lang")
    var lang: String = "",
     @SerializedName("status")
    var status: Status = Status(),
    @SerializedName("contributors_enabled")
    var contributorsEnabled: Boolean? = null,
    @SerializedName("is_translator")
    var isTranslator: Boolean? = null,
    @SerializedName("is_translation_enabled")
    var isTranslationEnabled: Boolean? = null,
    @SerializedName("profile_background_color")
    var profileBackgroundColor: String = "",
    @SerializedName("profile_background_image_url")
    var profileBackgroundImageUrl: String = "",
    @SerializedName("profile_background_image_url_https")
    var profileBackgroundImageUrlHttps: String = "",
    @SerializedName("profile_background_tile")
    var profileBackgroundTile: Boolean? = null,
    @SerializedName("profile_image_url")
    var profileImageUrl: String = "",
    @SerializedName("profile_image_url_https")
    var profileImageUrlHttps: String = "",
    @SerializedName("profile_link_color")
    var profileLinkColor: String = "",
    @SerializedName("profile_sidebar_border_color")
    var profileSidebarBorderColor: String = "",
    @SerializedName("profile_sidebar_fill_color")
    var profileSidebarFillColor: String = "",
    @SerializedName("profile_text_color")
    var profileTextColor: String = "",
    @SerializedName("profile_use_background_image")
    var profileUseBackgroundImage: Boolean? = null,
    @SerializedName("has_extended_profile")
    var hasExtendedProfile: Boolean? = null,
    @SerializedName("default_profile")
    var defaultProfile: Boolean? = null,
    @SerializedName("default_profile_image")
    var defaultProfileImage: Boolean? = null,
    @SerializedName("following")
    var following: Boolean= false,
    @SerializedName("follow_request_sent")
    var followRequestSent: Boolean? = null,
    @SerializedName("notifications")
    var notifications: Boolean? = null,
    @SerializedName("translator_type")
    var translatorType: String = "",
    @SerializedName("suspended")
    var suspended: Boolean? = null,
    @SerializedName("needs_phone_verification")
    var needsPhoneVerification: Boolean? = null
) {

    data class Entities(
        @SerializedName("url")
        var url: Url= Url(),
        @SerializedName("description")
        var description: Description = Description()
    ) {

        data class Description(
            @SerializedName("urls") var urls: List<Url.Url_> = arrayListOf()
        )

        data class Url(
            @SerializedName("urls")
            var urls: List<Url_> = arrayListOf()
        ) {

            data class Url_(
                @SerializedName("url")
                var url: String = "",
                @SerializedName("expanded_url")
                var expandedUrl: String = "",
                @SerializedName("display_url")
                var displayUrl: String = "",
                @SerializedName("indices")
                var indices: List<Long> = arrayListOf()
            )
        }
    }

    data class Status(
        @SerializedName("created_at")
        var createdAt: String = "",
        @SerializedName("id")
        var id: Long = 0L,
        @SerializedName("id_str")
        var idStr: String = "",
        @SerializedName("text")
        var text: String = "",
        @SerializedName("truncated")
        var truncated: Boolean? = null,
      /*  @SerializedName("entities")
        var entities: Entities_? = null,*/
        @SerializedName("source")
        var source: String = "",
        @SerializedName("in_reply_to_status_id")
        var inReplyToStatusId: Long = 0L,
        @SerializedName("in_reply_to_status_id_str")
        var inReplyToStatusIdStr: String = "",
        @SerializedName("in_reply_to_user_id")
        var inReplyToUserId: Long = 0L,
        @SerializedName("in_reply_to_user_id_str")
        var inReplyToUserIdStr: String = "",
        @SerializedName("in_reply_to_screen_name")
        var inReplyToScreenName: String = "",
      /*  @SerializedName("geo")
        var geo: String? = "",*/
        @SerializedName("coordinates")
        var coordinates: Any? = null,
        @SerializedName("place")
        var place: Any? = null,
        @SerializedName("contributors")
        var contributors: Any? = null,
        @SerializedName("is_quote_status")
        var isQuoteStatus: Boolean? = null,
        @SerializedName("retweet_count")
        var retweetCount: Long = 0L,
        @SerializedName("favorite_count")
        var favoriteCount: Long = 0L,
        @SerializedName("favorited")
        var favorited: Boolean? = null,
        @SerializedName("retweeted")
        var retweeted: Boolean? = null,
        @SerializedName("lang")
        var lang: String = ""
    ) {

      /*  data class Entities_(
            @SerializedName("hashtags")
            var hashtags: List<String>? = null,
            @SerializedName("symbols")
            var symbols: List<String>? = null,
            @SerializedName("user_mentions")
            var userMentions: List<String>? = null,
            @SerializedName("urls")
            var urls: List<Entities.Url.Url_>? = null
        )*/
    }
}