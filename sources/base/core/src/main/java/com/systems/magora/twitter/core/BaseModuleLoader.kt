package com.systems.magora.twitter.core

import org.koin.core.context.loadKoinModules
import org.koin.core.module.Module

abstract class BaseModuleLoader(private val modules: List<Module>) {

    private var isLoaded = false

    fun load(){
        if(!isLoaded){
            runCatching {
                loadKoinModules(modules)
            }
            isLoaded = true
        }
    }
}