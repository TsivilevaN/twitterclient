package com.systems.magora.twitter.core.model

import com.systems.magora.twitter.core.model.locale.AuthorizationData
import com.systems.magora.twitter.core.model.locale.User

fun List<User>.deleteMe(): List<User> {
    var newList = mutableListOf<User>()
    this.forEach {
        if (!it.isMe) newList.add(it)
    }
    return newList
}

fun User.isEmpty(): Boolean {
    return this.userName.isEmpty()
            && this.screenName.isEmpty()
            && this.avatarUrl.isEmpty()
            && this.bio.isEmpty()
}


