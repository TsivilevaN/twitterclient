package com.systems.magora.twitter.core

import android.content.Intent
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.systems.magora.twitter.core.model.locale.AuthorizationData
import com.systems.magora.twitter.core.model.locale.User

abstract class aseViewModel() : ViewModel() {
    abstract val currentUser: User
    abstract val authorizationData: LiveData<NetworkResponse<AuthorizationData>>
    abstract fun saveThreeFactorAuthorizationResults(requestCode: Int, resultCode: Int, data: Intent?)
}