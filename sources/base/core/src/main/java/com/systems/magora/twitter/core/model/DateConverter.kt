package com.systems.magora.twitter.core.model

import android.util.Log
import java.text.SimpleDateFormat
import java.util.*

fun String.convertFromNetwork(): Date {
    var date=Date(0L)
    val networkPattern = "EEE MMM dd HH:mm:ss +0000 yyyy"
    val networkFormat = SimpleDateFormat(networkPattern, Locale.US)
    networkFormat.timeZone = TimeZone.getTimeZone("RFC 822")
    try {
        date=networkFormat.parse(this)
    } catch (ex: Exception) {
       ex.printStackTrace()
    }finally {
        return date
    }
}


fun Date.toLocalDate():String{
    var localDateString=""
    val localPattern = "HH:mm - dd MMM, yyyy"
    val localFormat = SimpleDateFormat(localPattern, Locale.getDefault())
    localFormat.timeZone = TimeZone.getDefault()

    try {
        localDateString=localFormat.format(this)
    }catch (ex:Exception){
        ex.printStackTrace()
    }finally {
        return localDateString
    }
}


fun String.toLocalDate(): String {
    val networkPattern = "EEE MMM dd HH:mm:ss +0000 yyyy"
    val localPattern = "hh:mm a - dd MMM, yyyy"

    var localDate = ""
    try {
        Log.d("converters", "Converters: try convert network data $this ")
        val networkFormat = SimpleDateFormat(networkPattern, Locale.US)
        networkFormat.timeZone = TimeZone.getTimeZone("RFC 822")
        val networkDate: Date = networkFormat.parse(this)

        val localFormat = SimpleDateFormat(localPattern, Locale.getDefault())
        localFormat.timeZone = TimeZone.getDefault()
        localDate = localFormat.format(networkDate)

    } catch (ex: Exception) {
        ex.printStackTrace()
    } finally {
        Log.d("converters", "to local data $localDate")
        return localDate
    }
}