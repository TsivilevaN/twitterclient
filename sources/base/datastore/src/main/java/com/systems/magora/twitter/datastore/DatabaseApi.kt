package com.systems.magora.twitter.datastore

import androidx.room.*
import com.systems.magora.twitter.core.model.locale.AuthorizationData
import com.systems.magora.twitter.core.model.locale.Tweet
import com.systems.magora.twitter.core.model.locale.User

@Dao
abstract class UserApi {
    @Query("SELECT * FROM User")
    abstract suspend fun getUsers(): List<User>

    @Query("SELECT * FROM User LIMIT :count OFFSET :count*(:page-1) ")
    abstract suspend fun getUsersPage(count: Int, page: Int): List<User>

    @Query("SELECT * FROM User WHERE userId=:id")
    abstract suspend fun getUserById(id: Long): User?

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract suspend fun insertUser(user: User): Long

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract suspend fun insertUsers(users: List<User>)

    @Update
    abstract suspend fun updateUser(user: User)

    @Query("DELETE FROM User")
    abstract suspend fun deleteAllUsers()
}

@Dao
abstract class TweetsApi {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract suspend fun insertTweets(tweet: Tweet): Long

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract suspend fun insertTweets(tweets: List<Tweet>)

    //for tests
    @Query("SELECT * FROM Tweets WHERE tweetId=:id")
    abstract suspend fun getTweetsById(id: Long): Tweet

    @Query("SELECT * FROM Tweets WHERE userId=:id")
    abstract suspend fun getTweetsByUserId(id: Long): List<Tweet>?

    @Query("DELETE FROM Tweets")
    abstract suspend fun deleteAllTweets()

    //пагинация
    @Query(" SELECT * FROM Tweets WHERE tweetId<:tweetStartId ORDER BY date DESC LIMIT :limit  ")
    abstract suspend fun selectNextTweets(tweetStartId: Long, limit: Int): List<Tweet>

    @Query(" SELECT * FROM Tweets WHERE tweetId>:tweetStartId ORDER BY date DESC LIMIT :limit  ")
    abstract suspend fun selectPreviousTweets(tweetStartId: Long, limit: Int): List<Tweet>

    @Query(" SELECT * FROM Tweets ORDER BY date DESC LIMIT :limit")
    abstract suspend fun selectTweets(limit: Int): List<Tweet>

    @Query("UPDATE Tweets SET isFavorie=1 WHERE tweetId=:tweetId ")
    abstract suspend fun like(tweetId: Long)
}

@Dao
abstract class AuthorityApi {
    @Query("SELECT * FROM Authorization")
    abstract suspend fun selectAuthorityData(): AuthorizationData?

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract suspend fun insertAuthorityData(authority: AuthorizationData)
}