package com.systems.magora.twitter.datastore

import android.app.Application
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.systems.magora.twitter.core.model.locale.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext


@Database(
    entities = [User::class, Settings::class, Tweet::class, Followers::class, Friends::class, AuthorizationData::class],
    version = 1
)
@TypeConverters(value = [DateConverter::class])
abstract class TwitterDatabase : RoomDatabase() {
    abstract fun getUserApi(): UserApi
    abstract fun getTweetsApi(): TweetsApi
    abstract fun getAuthorityApi(): AuthorityApi
}


class TwitterDatabaseImpl(val application: Application) {
    suspend operator fun invoke(): TwitterDatabase = withContext(Dispatchers.IO) {
        val database = Room.databaseBuilder(
            application.applicationContext,
            TwitterDatabase::class.java,
            "twitter_database"
        ).build()
        initAuthorityData(database)
        return@withContext database
    }

    private suspend fun initAuthorityData(database: TwitterDatabase) {
        val authorityApi = database.getAuthorityApi()
        val storedAuthorityData = authorityApi.selectAuthorityData()

        if (storedAuthorityData == null) {
            authorityApi.insertAuthorityData(AuthorizationData())
        }
    }

}