package com.systems.magora.twitter.datastore.di
import com.systems.magora.twitter.datastore.TwitterDatabaseImpl
import com.systems.magora.twitter.datastore.UserApi
import org.koin.dsl.binds
import org.koin.dsl.module

val dataStorageModule = module {
    single { TwitterDatabaseImpl(get()) } binds arrayOf(UserApi::class)
}

