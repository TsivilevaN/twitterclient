package com.systems.magora.twitter.datastore

import android.content.Context
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import com.systems.magora.twitter.core.model.locale.Tweet
import com.systems.magora.twitter.core.model.locale.User
import kotlinx.coroutines.runBlocking
import org.junit.After
import org.junit.Assert
import org.junit.Before
import org.junit.Test

class TwitterDatabaseTest {
    private lateinit var database: TwitterDatabase
    private lateinit var userApi: UserApi
    private lateinit var tweetsApi: TweetsApi
    private lateinit var authApi: AuthorityApi
    private var testUser = User(
        userId = 1L,
        userName = "user1",
        screenName = "User One",
        status = "HEY",
        location = "usa",
        bio = "Test User 1",
        followersCount = 2,
        friendsCount = 3
    )


    @Before
    fun setUp() {
        val context = ApplicationProvider.getApplicationContext<Context>()
        database = Room.inMemoryDatabaseBuilder(context, TwitterDatabase::class.java).build()
        userApi = database.getUserApi()
        tweetsApi = database.getTweetsApi()
        authApi = database.getAuthorityApi()

    }

    @Test
    fun selectAllUsersTest() = runBlocking {
        userApi.insertUser(testUser)
        Assert.assertEquals(userApi.getUsers().last(), testUser)
    }


    @Test
    fun insertUserTest() = runBlocking {
        userApi.insertUser(testUser)
        Assert.assertEquals(userApi.getUserById(id = testUser.userId), testUser)
    }


    @Test
    fun updateUserTest() = runBlocking {
        userApi.insertUser(testUser)
        val newUser = testUser.apply { userName = "Two User" }
        userApi.updateUser(newUser)
        Assert.assertEquals("Two User", userApi.getUserById(testUser.userId)?.userName)
    }


    @Test
    fun testAuthority() = runBlocking {
        /*     val auth = Authorization(
                 userId = 1,
                 access_token = "token",
                 access_secret_token = "wdwd",
                 consumer_key = "klkl"
             )*/
        print(authApi.selectAuthorityData()?.toString())
        //   Assert.assertEquals(userApi.selectAuthorityData(), auth)
    }


    @Test
    fun testTweetsInsert() = runBlocking {
        val tweet = Tweet(tweetId = 15)
        userApi.insertUser(testUser)
        tweetsApi.insertTweets(tweet)
        Assert.assertEquals(tweet, tweetsApi.getTweetsByUserId(tweet.tweetId)?.get(0))
    }

    @Test
    fun testLike()= runBlocking{
        val tweet=Tweet(tweetId = 1000,isFavorie = false)
        tweetsApi.insertTweets(tweet)
        var _tweet=tweetsApi.like(tweet.tweetId)
        var likedTweet=tweetsApi.getTweetsById(tweet.tweetId)
        Assert.assertTrue(likedTweet.isFavorie)

    }

    @After
    fun cleanUp() {

    }
}