package com.systems.magora.twitter.client.mainActivity

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.children
import androidx.navigation.findNavController
import androidx.navigation.ui.NavigationUI
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import com.systems.magora.twitter.client.NavMainDirections
import com.systems.magora.twitter.client.R
import com.systems.magora.twitter.core.BaseActivity
import com.systems.magora.twitter.core.getCurrentUserId
import com.systems.magora.twitter.core.interfaces.BottomBarHideable
import kotlinx.android.synthetic.main.activity_main.*
import org.koin.androidx.viewmodel.ext.android.viewModel


class MainActivity :
    BaseActivity(),
    BottomBarHideable {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setUpBottomNavigationView()
    }

    private fun setUpBottomNavigationView() {
        val navController = findNavController(R.id.main_nav_host)
        NavigationUI.setupWithNavController(bottom_navigation_view, navController)

        bottom_navigation_view.setOnNavigationItemSelectedListener { _menuItem ->
            val currentTabID = bottom_navigation_view.selectedItemId
            val currentUser=Firebase.auth.getCurrentUserId()
            makeTabAvailable(currentTabID)
            when (_menuItem.itemId) {
                //TODO раскоментить поле сборки
                R.id.profileNavHost ->{
                    currentUser?.let {
                    navController.navigate(
                        NavMainDirections.actionGlobalProfileNavHost(it)
                    )}}
                R.id.tweetsNavHost->{
                        currentUser?.let {
                            NavMainDirections.actionGlobalTweetsNavHost(
                                it
                            )
                        }
                    }

                R.id.searchNavHost -> navController.navigate(NavMainDirections.actionGlobalSearchNavHost())
            }

            false
        }
    }

    private fun makeTabAvailable(itemId: Int) {
        bottom_navigation_view.children.forEach {
            it.isEnabled = it.id == itemId
        }
    }



    override fun hide() {
        bottom_navigation_view.visibility = View.GONE
    }

    override fun show() {
        bottom_navigation_view.visibility = View.VISIBLE
    }


}
