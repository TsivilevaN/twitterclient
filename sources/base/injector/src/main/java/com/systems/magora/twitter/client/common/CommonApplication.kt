package com.systems.magora.twitter.client.common

import android.app.Application
import com.systems.magora.twitter.datastore.di.dataStorageModule
import com.systems.magora.twitter.networking.di.networkModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class CommonApplication : Application() {
    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidContext(this@CommonApplication)
            modules(
                dataStorageModule,
                networkModule,
                navigationModule
            )
        }
    }
}