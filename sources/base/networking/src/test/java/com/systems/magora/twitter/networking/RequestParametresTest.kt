package com.systems.magora.twitter.networking

import com.systems.magora.twitter.core.model.locale.AuthorizationData
import org.junit.After
import org.junit.Before
import org.junit.Test

class RequestParametresTest {

    @Before
    fun setUp() {
    }

    @After
    fun tearDown() {
    }

    @Test
    fun testSigned() {
        val signed = RequestParametres.Builder(
            BaseUrls.PROFILE,
            AuthorizationData(userId = 1111, access_token = "fkmlm", access_secret_token = "klmlmml")
        ).userId(1243492248219717632).count(5).cursor(908900).query("8").build().getSignature()
        println(signed)
    }
}