package com.systems.magora.twitter.networking.di

import com.systems.magora.twitter.networking.RemoteRepositoryImpl
import com.systems.magora.twitter.networking.clients.TwitterClient
import com.systems.magora.twitter.networking.repos.RemoteRepository
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.dsl.binds
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

val networkModule= module {
    val loggingInterceptor = HttpLoggingInterceptor().apply {
        level = HttpLoggingInterceptor.Level.BODY
    }
    val client = OkHttpClient.Builder()
        .addInterceptor(loggingInterceptor)
        .retryOnConnectionFailure(true)
        .build()

    val retrofit = Retrofit.Builder()
        .baseUrl("https://api.twitter.com/1.1/")
        .addConverterFactory(GsonConverterFactory.create())
        .client(client)
        .build()
    single { RemoteRepositoryImpl(retrofit.create(TwitterClient::class.java),get()) } binds arrayOf(RemoteRepository::class)
}