package com.systems.magora.twitter.networking

import com.github.scribejava.core.model.Verb
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import com.systems.magora.twitter.core.NetworkError
import com.systems.magora.twitter.core.NetworkResponse
import com.systems.magora.twitter.core.NetworkSuccessfull
import com.systems.magora.twitter.core.model.deleteMe
import com.systems.magora.twitter.core.model.locale.AuthorizationData
import com.systems.magora.twitter.core.model.locale.Tweet
import com.systems.magora.twitter.core.model.locale.User
import com.systems.magora.twitter.core.model.toTweets
import com.systems.magora.twitter.core.model.toUser
import com.systems.magora.twitter.core.model.toUserList
import com.systems.magora.twitter.datastore.TwitterDatabaseImpl
import com.systems.magora.twitter.networking.clients.TwitterClient
import com.systems.magora.twitter.networking.repos.RemoteRepository
import retrofit2.await
import kotlin.reflect.KClass

class RemoteRepositoryImpl(
    private val remoteClient: TwitterClient,
    private val database: TwitterDatabaseImpl
) : RemoteRepository {

    private suspend fun isMyProfile(userId: Long): Boolean {
        val myId = selectAuthority()?.userId
        return myId == userId
    }


    override suspend fun selectAuthority() = database
        .invoke()
        .getAuthorityApi()
        .selectAuthorityData()
    override suspend fun saveAuthorityInfo(authorizationData: AuthorizationData) {
        database.invoke().getAuthorityApi().insertAuthorityData(authorizationData)
    }



    override suspend fun getTweets(count: Int): NetworkResponse<List<Tweet>> {
        val dbApi = database.invoke().getTweetsApi()
        return try {
            val authority = selectAuthority()
            authority?.let {

            }
            val signature = RequestParametres
                .Builder(BaseUrls.HOME_TIMELINE, selectAuthority()!!)
                .count(count)
                .build()
                .getSignature(Verb.GET)

            val tweetsList = remoteClient
                .getTweets(signature.url, signature.header)
                .await()
                .toTweets()

            dbApi.insertTweets(tweetsList)
            NetworkSuccessfull(tweetsList)

        } catch (ex: Exception) {
            val cached = dbApi.selectTweets(count)
            NetworkError(error = ex, cachedData = cached)
        }
    }

    override suspend fun getTweetsBefore(count: Int, startId: Long): NetworkResponse<List<Tweet>> {
        val dbApi = database.invoke().getTweetsApi()
        return try {
            val signature = RequestParametres
                .Builder(BaseUrls.HOME_TIMELINE, selectAuthority()!!)
                .count(count)
                .sinceId(startId)
                .build()
                .getSignature(Verb.GET)

            val tweetsList = remoteClient
                .getTweets(signature.url, signature.header)
                .await()
                .toTweets()

            dbApi.insertTweets(tweetsList)
            NetworkSuccessfull(tweetsList)

        } catch (ex: Exception) {
            val cached = dbApi.selectPreviousTweets(startId, count)
            NetworkError(ex, cached)
        }
    }

    override suspend fun getTweetsAfter(count: Int, maxId: Long): NetworkResponse<List<Tweet>> {
        val dbApi = database.invoke().getTweetsApi()
        return try {
            val signature = RequestParametres
                .Builder(BaseUrls.HOME_TIMELINE, selectAuthority()!!)
                .count(count)
                .maxId(maxId - 1)
                .build()
                .getSignature(Verb.GET)

            val data = remoteClient
                .getTweets(signature.url, signature.header)
                .await()
                .toTweets()

            NetworkSuccessfull(data)
        } catch (ex: Exception) {
            val cached = dbApi.selectNextTweets(maxId, count)

            NetworkError(ex, cached)
        }
    }

    override suspend fun searchUser(
        name: String,
        count: Int,
        pageNumber: Int
    ): NetworkResponse<List<User>> {
        val dbApi = database.invoke().getUserApi()
        return try {
            val signedUrl = RequestParametres.Builder(BaseUrls.SEARCH_USER, selectAuthority()!!)
                .count(count)
                .page(pageNumber)
                .query(name)
                .build()
                .getSignature(Verb.GET)

            var userList =
                remoteClient.searchUsers(signedUrl.url, signedUrl.header).await().toUserList()
            userList = userList.deleteMe()

            dbApi.insertUsers(userList)
            NetworkSuccessfull(userList)

        } catch (e: Exception) {
            val userList = dbApi.getUsersPage(count, pageNumber).deleteMe()
            NetworkError(e, userList)
            //NetworkFaile(e,null)
        }

    }


    override suspend fun likeTweet(tweetId: Long): Boolean {
        val dbApi = database.invoke().getTweetsApi()
        return try {
            dbApi.like(tweetId)

            val oauth = RequestParametres
                .Builder(baseUrls = BaseUrls.SET_LIKE, authorizationData = selectAuthority()!!)
                .tweetId(tweetId)
                .build()
                .getSignature(method = Verb.POST)
            remoteClient.like(oauth.url, oauth.header)
            true
        } catch (e: Exception) {
            false
        }

    }

    override suspend fun dislikeTweet(tweetId: Long): Boolean {
        val dbApi = database.invoke().getTweetsApi()
        return try {
            dbApi.like(tweetId)

            val oauth = RequestParametres
                .Builder(baseUrls = BaseUrls.DISLIKE, authorizationData = selectAuthority()!!)
                .tweetId(tweetId)
                .build()
                .getSignature(method = Verb.POST)
            remoteClient.like(oauth.url, oauth.header)
            true
        } catch (e: Exception) {
            false
        }
    }


    override suspend fun getProfileInfo(userId: Long): NetworkResponse<User> {
        val dbApi = database.invoke().getUserApi()
        var user: User?
        return try {
            val oauth = RequestParametres
                .Builder(baseUrls = BaseUrls.PROFILE, authorizationData = selectAuthority()!!)
                .userId(userId)
                .build()
                .getSignature(method = Verb.GET)

            user = remoteClient.getProfilePojo(oauth.url, oauth.header).await().toUser()

            if (isMyProfile(userId)) {
                user.isMe = true
            }
            dbApi.insertUser(user)
            NetworkSuccessfull(user)

        } catch (e: Exception) {
            user=dbApi.getUserById(userId)
            NetworkError(e, user)
        }
    }

    override suspend fun saveProfileInfo(user: User): Long {
        val dbApi = database.invoke().getUserApi()
        return dbApi.insertUser(user)
    }




    override suspend fun cleareCache(clazz: KClass<*>) {
        when (clazz) {
            User::class -> {
                database.invoke().getUserApi().deleteAllUsers()
            }
            Tweet::class -> {
                database.invoke().getTweetsApi().deleteAllTweets()
            }

        }
    }

}