package com.systems.magora.twitter.networking

import com.github.scribejava.apis.TwitterApi
import com.github.scribejava.core.builder.ServiceBuilder
import com.github.scribejava.core.model.OAuth1AccessToken
import com.github.scribejava.core.model.OAuthRequest
import com.github.scribejava.core.model.Verb
import com.systems.magora.twitter.core.model.locale.AuthorizationData

enum class BaseUrls(val value: String) {
    PROFILE("https://api.twitter.com/1.1/users/show.json?"),
    HOME_TIMELINE("https://api.twitter.com/1.1/statuses/home_timeline.json?"),
    SEARCH_USER("https://api.twitter.com/1.1/users/search.json?"),
    SET_LIKE("https://api.twitter.com/1.1/favorites/create.json?"),
    DISLIKE("https://api.twitter.com/1.1/favorites/destroy.json?")
}

data class OAuth(val url: String, val header: String)

data class RequestParametres
private constructor(
    val baseUrl: BaseUrls,
    val authorizationDataData: AuthorizationData,
    val userId: Long?,
    val tweetId:Long?,
    val count: Int? = 20,
    val cursor: Long?,
    val query: String?, /* текст поискового запроса*/
    val maxId: Long?,
    val sinceId:Long?,
    val page:Int?
) {
    private fun convertParamsToString(): String {
        var result = ""
        if (userId != null) result = "&user_id=${userId}"
        if (tweetId != null) result = "&id=${tweetId}"
        if (count != null) result += "&count=$count"
        if (cursor != null) result += "&next_cursor=$cursor"
        if (query != null) result += "&q=$query"
        if (maxId != null) result += "&max_id=$maxId"
        if (sinceId != null) result += "&since_id=$sinceId"
        if(page!=null) result+="&page=$page"

        if (result.length > 0) {
            if (result[0].equals('&')) {
                result = result.substring(1, result.length)
            }
            if (result[result.lastIndex] == '&') {
                result = result.substring(0, result.length - 1)
            }
        }
        return result
    }

    class Builder(val baseUrls: BaseUrls, var authorizationData: AuthorizationData) {
        private var userId: Long? = null
        private var tweetId:Long?=null
        private var count: Int? = null
        private var cursor: Long? = null
        private var query: String? = null
        private var maxId: Long? = null
        private var sinceId: Long? = null
        private var page: Int? = null


        fun userId(value: Long?): Builder {
            userId = value
            return this
        }

        fun tweetId(value: Long?): Builder {
            tweetId = value
            return this
        }

        fun count(value: Int?): Builder {
            count = value
            return this
        }

        fun cursor(value: Long?): Builder {
            cursor = value
            return this
        }

        fun query(value: String): Builder {
            query = value
            return this
        }

        fun maxId(value: Long): Builder {
            maxId = value
            return this
        }


        fun sinceId(value: Long): Builder {
            sinceId = value
            return this
        }
        fun page(value: Int): Builder {
            page = value
            return this
        }

        fun build(): RequestParametres {
            return RequestParametres(
                baseUrl = baseUrls,
                authorizationDataData = authorizationData,
                userId = userId,
                tweetId = tweetId,
                count = count,
                cursor = cursor,
                query = query,
                maxId = maxId,
                sinceId = sinceId,
                page = page
            )
        }
    }

    fun getSignature(method: Verb): OAuth {
        val fullUrl = this.baseUrl.value + this.convertParamsToString()
        return OAuth(
            url = fullUrl,
            header = getAuthorityHeader(method, fullUrl, this.authorizationDataData)
        )
    }
}


private fun getAuthorityHeader(
    method: Verb, link: String, authorizationData: AuthorizationData
): String {
    val service = ServiceBuilder(authorizationData.consumer_key)
        .apiSecret(authorizationData.consumer_secret_token)
        .build(TwitterApi.instance())
    val request = OAuthRequest(method, link)

    service.signRequest(
        OAuth1AccessToken(
            authorizationData.access_token,
            authorizationData.access_secret_token
        ),
        request
    )
    return request.headers["Authorization"]!!
}

