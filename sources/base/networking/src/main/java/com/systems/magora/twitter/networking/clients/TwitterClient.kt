package com.systems.magora.twitter.networking.clients

import com.systems.magora.twitter.core.model.remote.ProfilePojo
import com.systems.magora.twitter.core.model.remote.TweetsPojo
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.POST
import retrofit2.http.Url

interface TwitterClient {
    @GET
    fun getProfilePojo(@Url link: String, @Header("Authorization") header: String): Call<ProfilePojo>
    @GET
    fun getTweets(@Url link: String, @Header("Authorization") header: String): Call<List<TweetsPojo>>
    @GET
    fun searchUsers(@Url link: String, @Header("Authorization") header: String): Call<List<ProfilePojo>>
    @GET
    suspend fun getPageOfTweets(@Url link: String, @Header("Authorization") header: String): List<TweetsPojo>

    @POST
    suspend fun like(@Url link: String, @Header("Authorization") header: String)

}

    //https://twitter.com/TsivilyovaN/profile_image?size=original
