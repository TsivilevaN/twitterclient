package com.systems.magora.twitter.networking.repos

import com.systems.magora.twitter.core.NetworkResponse
import com.systems.magora.twitter.core.model.locale.AuthorizationData
import com.systems.magora.twitter.core.model.locale.Tweet
import com.systems.magora.twitter.core.model.locale.User
import kotlin.reflect.KClass

interface RemoteRepository {
    suspend fun selectAuthority(): AuthorizationData?
    suspend fun saveAuthorityInfo(authorizationData: AuthorizationData)

    suspend fun saveProfileInfo(user:User) : Long
    suspend fun getProfileInfo(userId: Long): NetworkResponse<User>

    suspend fun cleareCache(clazz: KClass<*>)

    suspend fun getTweets(count: Int): NetworkResponse<List<Tweet>>
    suspend fun getTweetsBefore(count: Int, startId: Long): NetworkResponse<List<Tweet>>
    suspend fun getTweetsAfter(count: Int, maxId: Long): NetworkResponse<List<Tweet>>

    suspend fun searchUser(name: String, count: Int,pageNumber: Int): NetworkResponse<List<User>>

    suspend fun likeTweet(tweetId:Long):Boolean
    suspend fun dislikeTweet(tweetId:Long):Boolean
}